#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

typedef std::pair<std::vector<std::string>, int> PERMISSION_PAIR;

PERMISSION_PAIR getPermissionPair(const std::string &permission) {
    auto split = [=](const std::string &str, char sc) {
        std::vector<std::string> vec_string;
        std::string::size_type pos1, pos2;
        pos2 = str.find(sc);
        pos1 = 0;
        while (std::string::npos != pos2) {
            vec_string.push_back(str.substr(pos1, pos2 - pos1));
            pos1 = pos2 + 1;
            pos2 = str.find(sc, pos1);
        }
        vec_string.push_back(str.substr(pos1));
        return vec_string;
    };
    PERMISSION_PAIR permission_pair;
    auto group_other_vector = split(permission, ';');
    auto group_vector = split(group_other_vector.at(0), ':');
    permission_pair.first = group_vector;
    if (group_other_vector.size() > 1)
        permission_pair.second = std::stoi(group_other_vector.at(1));
    else
        permission_pair.second = 0;
    return permission_pair;
}

int comparePermissionPair(const PERMISSION_PAIR _1, const PERMISSION_PAIR _2) {
    if (_1.second || _2.second)
        return 1;
    for (auto it = _1.first.begin(); it != _1.first.end(); it++) {
        if (std::find(_2.first.begin(), _2.first.end(), *it) != _2.first.end())
            return 1;
    }
    return 0;
}

std::vector<std::string>
findVehicle(const PERMISSION_PAIR from, const PERMISSION_PAIR to, std::vector<PERMISSION_PAIR> vehicle_permission_pair,
            std::vector<std::string> vehicle_names) {
    std::vector<std::string> vehicle_candidate;

    if (vehicle_permission_pair.size() != vehicle_names.size())
        throw std::runtime_error("Size not match");

    for (int i = 0; i < vehicle_permission_pair.size(); ++i) {
        if (comparePermissionPair(from, vehicle_permission_pair.at(i)) &&
            comparePermissionPair(to, vehicle_permission_pair.at(i))) {
            vehicle_candidate.push_back(vehicle_names.at(i));
        }
    }
    return vehicle_candidate;
}

int main() {
    std::cout << "Hello, World!" << std::endl;

    std::string EQP_A_0{"A;0"};
    std::string EQP_A_1{"A;1"};

    std::string EQP_B_0{"B;0"};
    std::string EQP_B_1{"B;1"};

    std::string EQP_AB_0{"A:B;0"};
    std::string EQP_AB_1{"A:B;1"};

    std::string EQP_C_0{"C;0"};
    std::string EQP_C_1{"C;1"};

    std::string Vehicle_A{"A"};
    std::string Vehicle_B{"B"};
    std::string Vehicle_C{"A:B"};

    std::vector<std::string> vehicles;
    vehicles.push_back(Vehicle_A);
    vehicles.push_back(Vehicle_B);
    vehicles.push_back(Vehicle_C);

    std::vector<PERMISSION_PAIR> permission_pair_vehicles;
    for (auto vehicle : vehicles) {
        permission_pair_vehicles.push_back(getPermissionPair(vehicle));
    }

    auto vehicle_candidate = findVehicle(getPermissionPair(EQP_A_0), getPermissionPair(EQP_C_1),
                                         permission_pair_vehicles,
                                         vehicles);

    if (vehicle_candidate.empty())
        std::cerr << "Not match vehicle" << std::endl;

    for (auto vehicle : vehicle_candidate) {
        std::cout << "Vehicle id: " << vehicle << std::endl;
    }


//    for (auto vehicle : vehicles) {
//        std::cout << "EQP_A_0 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_A_0), getPermissionPair(vehicle))
//                  << std::endl;
//
//        std::cout << "EQP_A_1 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_A_1), getPermissionPair(vehicle))
//                  << std::endl;
//
//        std::cout << "EQP_B_0 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_B_0), getPermissionPair(vehicle))
//                  << std::endl;
//
//        std::cout << "EQP_B_1 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_B_1), getPermissionPair(vehicle))
//                  << std::endl;
//
//        std::cout << "EQP_AB_0 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_AB_0), getPermissionPair(vehicle))
//                  << std::endl;
//
//        std::cout << "EQP_AB_1 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_AB_1), getPermissionPair(vehicle))
//                  << std::endl;
//
//        std::cout << "EQP_C_0 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_C_0), getPermissionPair(vehicle))
//                  << std::endl;
//
//        std::cout << "EQP_C_1 <- " << vehicle << " Ans: "
//                  << comparePermissionPair(getPermissionPair(EQP_C_1), getPermissionPair(vehicle))
//                  << std::endl;
//    }

    return 0;
}
